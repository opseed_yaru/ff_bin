# ff_bin

# make_array_of_images

```js
if(0)return exec_post_data();
var src="var resp_off=()=>{};var response={write:s=>require('process').stdout.write(s),end:()=>{}};var POST={};\n"+(fs.readFileSync("main.js")+"").split("var hosts={};")[0]+POST.code.split("\n").slice(2).join("\n");fs.writeFileSync("src.js",src);//return src;

var without_resp=!('code' in POST);

//return inspect(process);
//if(!without_resp)return exec_post_data();

      var exec_with_stream=(cmd,stream,cb)=>{
        if(typeof stream!=='object')throw Error('no way. // atm "typeof stream" = '+(typeof stream));
        if(typeof stream.write!=='function')throw Error('no way. // atm "typeof stream.write" = '+(typeof stream.write));
        var to_stream=s=>stream.write(s);
        var p=spawn('bash',[]);
        p.stdin.end(cmd+"\n");
        p.stdout.on('data',to_stream);
        p.stderr.on('data',to_stream);
        p.on('exit',cb?()=>cb(stream):()=>stream.end());
        return p;
      }

      var hack_require=((res)=>{var r=res;return (m,tarball)=>{
        try{require.resolve(m);}catch(e){
          r.write(m+" is not found, but ok, i already run 'npm install "+m+"'\n\n");
          exec_with_stream("echo npm install "+(tarball?tarball:m)+"\n npm install "+m,r);
          return false;//throw new Error('hack_require.fail');
        }
        return require(m);
      };})(response);

var Jimp=hack_require('jimp');//var ffmpeg=hack_require('fluent-ffmpeg');
//return Jimp+"";
//return txt+"";

//var arr=[];for(var i=0;i<100;i++)arr.push(i);return arr.map(e=>pad(e,10)).join("\n");
var pad=(v,n)=>('000000000'+v).substr(-n);

var sqr=x=>x*x;
var vec2d=(x,y)=>({x,y});
var rnd=()=>Math.random();
var rndvec2d=()=>vec2d(rnd(),rnd());
var mul=(v,k)=>vec2d(v.x*k,v.y*k);
var add=(p,v)=>vec2d(p.x+v.x,p.y+v.y);

var pix=[0xff000000/*,0xff000000,0xff0000ff*/];
//var n=1024;var dx=n;var dy=n;
var scale=1.0;
var world={t:0,wh:mul(vec2d(1920,1200),scale),parr:[]};
var wh=world.wh;for(var i=0;i<199;i++)world.parr.push({v:mul(add(rndvec2d(),vec2d(-0.5,-0.5)),1),p:vec2d(rnd()*wh.x,rnd()*wh.y)});

if(without_resp){
  var world_txt=""+fs.readFileSync(0);
  if(!world_txt.length)qap_log("input of stdin is empty :(\ncontent of world as json expected");
  world=JSON.parse(world_txt);
}else{
  fs.writeFileSync("world.json",json(world));
  var out=[];
  for(var i=0;i<256;i++){
    world.t=i;
    world.parr.map(e=>e.p=add(e.p,e.v));
    out.push(json(world));
  }
  out=out.reverse();//return inspect(out);
  var tarr=[1,2,3,4,5];
  var go=tid=>()=>{if(out.length)run(out.pop(),tid);}
  var run=(e,tid)=>{
    var p=spawn('node',['src.js',JSON.parse(e).t,tid+""]);
    p.stdin.end(e);
    p.on('exit',go(tid));
  }
  tarr.map(e=>go(e)());
  //return exec_post_data();
  return ""+execSync("cat world.json");
}

// add(mul(rndvec2d(),n),vec2d(+0.0*n,+0.0*n))
//return inspect(world);

var r=response;resp_off();

var cb=(world,fn)=>(err,image)=>{
  if(err){txt(inspect(err+""));}
  var wh=world.wh;var hwh=mul(wh,0.5);var dx=wh.x;var dy=wh.y; 
  var argb=(a,r,g,b)=>Jimp.rgbaToInt(b,g,r,a);
  var get=v=>Jimp.intToRGBA(v);
  var argb2rgba=v=>{
    var a=(v>>24)&0xff;
    var r=(v>>16)&0xff;
    var g=(v>>8 )&0xff;
    var b=(v    )&0xff;
    return Jimp.rgbaToInt(r,g,b,a);
  };
  var main=()=>{
    var h=dx/2;var r=128*scale;
    var p=vec2d(h,h);
    for(var x=0;x<dx;x++)for(var y=0;y<dy;y++){
      //var r=h*2;
      var qq=qapmin(world.parr,e=>sqr(x-e.p.x)+sqr(y-e.p.y));
      qq=Math.sqrt(qq)/r;
      var fail=qq>1;
      qq=1.0-(qq>1?1:qq);
      qq=Math.cos((1.0-qq)*Math.PI*0.5);
      var q=(qq*255)|0;
      var c=Jimp.rgbaToInt(q,q,q,255);
      pix.map((v,i)=>image.setPixelColor(c,x+i*dx,y+0));
      if(fail)pix.map((v,i)=>image.setPixelColor(argb2rgba(v),x+i*dx,y+0));
    }
  };
  main();
  image/*.gaussian(0)*/.write(fn);
  setTimeout(()=>{
    if(without_resp)return;
    fs.stat(fn,(error,stat)=>{
      if(error){throw error;}
      var arr=contentTypesByExtension;
      var ext=path.extname(fn);
      var ct=ext in arr?arr[ext]:'application/octet-stream';
      r.writeHead(200,{'Content-Type':ct,'Content-Length':stat.size})
      fs.createReadStream(fn).pipe(r).on('end',()=>{r.destroy();request.destroy()});
    });
  },500);
}
var render=(world,fn)=>{
  new Jimp(pix.length*wh.x,wh.y,0xff0000ff,cb(world,fn));
}
render(world,"img"+pad(world.t,10)+".png");
if(!without_resp)return "ok";
```

# make_video
```bash
if [ ! -f "ffmpeg" ]; then
  wget https://ff-bin.now.sh/ffmpeg.zip
  unzip ffmpeg.zip
  chmod +x ffmpeg
  echo "wget-ok"
  ls -lht|grep ffmpeg
fi

./ffmpeg -framerate 60 -pattern_type glob -i 'img*.png' -c:v libx264 -c:a copy -shortest -r 60 -pix_fmt yuv420p raw.mp4
```

# ffmpeg_src_link
https://johnvansickle.com/ffmpeg/releases/ffmpeg-release-amd64-static.tar.xz

TODO: ...